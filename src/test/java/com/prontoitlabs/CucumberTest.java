package com.prontoitlabs;

import org.junit.Test;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
			features = {"src/test/resources/text.feature"},
			format = {"pretty","html:target/"},
			tags = {"@Test"}
			)
public class CucumberTest {
@Test
	public void check() throws InterruptedException{
	    StepDefinition.getInstance().navigate_to_the_facebook_login_page();;
		Thread.sleep(1000);
		StepDefinition.getInstance().user_logs_in_with_the_username_and_password();
		Thread.sleep(1000);
		StepDefinition.getInstance().home_page_should_be_displayed();
		}
}
