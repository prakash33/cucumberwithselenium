$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/text.feature");
formatter.feature({
  "id": "facebook-login-functionality",
  "description": "",
  "name": "Facebook login functionality",
  "keyword": "Feature",
  "line": 1
});
formatter.scenario({
  "id": "facebook-login-functionality;as-a-user,i-should-be-able-to-login-into-my-facebook-account.",
  "tags": [
    {
      "name": "@Test",
      "line": 2
    }
  ],
  "description": "",
  "name": "As a user,I should be able to login into my facebook account.",
  "keyword": "Scenario",
  "line": 3,
  "type": "scenario"
});
formatter.step({
  "name": "Navigate to the facebook login page.",
  "keyword": "Given ",
  "line": 4
});
formatter.step({
  "name": "User logs in with the username and password.",
  "keyword": "When ",
  "line": 5
});
formatter.step({
  "name": "Home page should be displayed.",
  "keyword": "Then ",
  "line": 6
});
formatter.match({
  "location": "StepDefinition.navigate_to_the_facebook_login_page()"
});
formatter.result({
  "duration": 6109540484,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.user_logs_in_with_the_username_and_password()"
});
formatter.result({
  "duration": 2234897517,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.home_page_should_be_displayed()"
});
formatter.result({
  "duration": 10032006278,
  "status": "passed"
});
});